<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacebookPage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('facebook_page', function (Blueprint $table) {
            $table->increments('id');
            $table->string("access_token");
            $table->string("category");
            $table->string("name");
            $table->string("facebook_id");
            $table->string("facebook_userid");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
