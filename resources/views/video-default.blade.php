<div class="videosImages">
  <ul class="equal-height-thumbnail">
@foreach($posts as $post)
	<li>
		  <a href="/video/{{$post->slug}}">   
   		  <figure><img src="https://fiska.hypedgamers.com/uploads/{{$post->thumbnail}}?w=420" alt="{{$post->title}}"></figure>
   		  <div class="caption">
   		   <h2>{{$post->title}} </h2> 
   		  </div>
		</a>
      <p class="date-paragraph"> {{ date('F d, Y', strtotime($post->created_at)) }} </p>
  </li>

	@endforeach
</ul>    