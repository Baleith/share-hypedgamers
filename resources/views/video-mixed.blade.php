<div class="videosImages">
   	<!--<div class="main-header">
   		<h1> Hypedgamers - Videos and Lists </h1>
   	</div>  main-header -->
@foreach($posts as $post)
   <div class="videoImg">
    	<div class="thumb">
			<a href="@if($post->type==='posts')
		/video/
		@else
		/list/
		@endif
{{$post->slug}}">
				<span class="play">&#9658;</span>
				<div class="overlay"></div>
			</a>
			<img border="0" alt="{!!$post->title!!}" class="lazy" data-src="http://fiska.hypedgamers.com/uploads/{{$post->thumbnail}}?w=343&h=193">
		</div>
		<a href="
		@if($post->type==='posts')
		/video/
		@else
		/list/
		@endif

		{{$post->slug}}"> {{$post->title}} </a>

		<p> {{ date('F d, Y', strtotime($post->created_at)) }} </p>
	</div>

	@endforeach
</div>