@extends('layout')

@section('content')

<div id="videoContent">

	<div class="videoClickedContainer">
		<div class="sense-video">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- över video -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-3847496997154634"
     data-ad-slot="1665368106"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
		</div> <!-- sense-->
	<div class="videoClicked">

		<h1 class="VideoClickedTitle">{{$post->title}}</h1>

		<div class="embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="{{$post->videoUrl}}"> </iframe>
		</div>

		<!-- <video class="video-js vjs-big-play-centered" controls data-setup="{}">
			<source src="{{URL::asset('Sorry.mp4')}}" type="video/mp4">
		</video>	
 -->
		<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=818252901657743";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</div>

	<div class="videoinfo">
		<p class="infoText"> Game : {{$post->category->name}} </p>
		<p class="infoText">Posted : {{ date('F d, Y', strtotime($post->created_at)) }}</p>
		<hr> </hr>
			{!!$post->content!!}
	</div>

<div class="fb-like" data-href="https://www.facebook.com/Hypedgamers/" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="true"></div>
	<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']}}"> 
		<div class="fb-share">
			Share on Facebook
		</div> <!-- fb-share --> 
	</a>


	</div>
	<div class="related">
		@include('video-default') 
 	</div>
@stop