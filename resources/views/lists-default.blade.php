<div class="videosImages">
@foreach($posts as $post)
   <div class="videoImg">
   	<div class="main-header">
   		<h1> Lists </h1>
   	</div> <!-- main-header -->
    	<div class="thumb">
			<a href="/list/{{$post->slug}}">
				<span class="play">&#9658;</span>
				<div class="overlay"></div>
			</a>
			<img border="0" alt="{!!$post->title!!}" class="lazy" data-src="http://fiska.hypedgamers.com/uploads/{{$post->thumbnail}}?w=343&h=193">
		</div>
		<a href="/list/{{$post->slug}}" class="unveil" > {{$post->title}} </a>

		<p> {{ date('F d, Y', strtotime($post->created_at)) }} </p>
	</div>

	@endforeach
</div>