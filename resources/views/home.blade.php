@extends('layout')

@section('content')


<div id="sidebar3"> 
	<nav>
	  <ul class="sideUL">             
	  	<li class="filter" filter="newest-mixed" >
	  		<a class="currentLink" href="#">Newest<i class="fa fa-external-link-square" aria-hidden="true"></i></a>
	  	</li>
 		<li class="filter" filter="popular-mixed" >
 		<a href="#">Popular <i class="fa fa-fire" aria-hidden="true"></i></a>
 		</li>
 		<li class="filter" filter="trending-mixed">
 			<a href="#">Trending <i class="fa fa-line-chart" aria-hidden="true"></i></a>
 		</li>
	  </ul>
	</nav>
</div>

	
@include('video-default') 


@stop