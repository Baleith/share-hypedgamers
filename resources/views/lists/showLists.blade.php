@extends('layout')

@section('content')

<div class="lists-container">
   	<div class="main-header">
   		<h1> Lists </h1>
   	</div> <!-- main-header -->
@foreach($lists as $list)

   <div class="videoImg">
    	<div class="thumb">
			<a href="/list/{{$list->slug}}">
				<span class="play">&#9658;</span>
				<div class="overlay"></div>
			</a>
			<img border="0" alt="{{$list->title}}" src="//fiska.hypedgamers.com/uploads/{{$list->thumbnail}}?w=343&h=193">
		</div>
		<a href="/list/{{$list->slug}}"> {{$list->title}} </a>

		<p> {{ date('F d, Y', strtotime($list->created_at)) }} </p>
	</div>

	@endforeach
</div>

@stop

