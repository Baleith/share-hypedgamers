@extends('layout')

@section('content')

<div id="videoContent">


	<div class="list-main-container">

		<h1 class="VideoClickedTitle">{{$list->title}}</h1>
		<p class="infoText">Posted : {{ date('F d, Y', strtotime($list->created_at)) }}</p>
		<hr> </hr>
		<!-- <video class="video-js vjs-big-play-centered" controls data-setup="{}">
			<source src="{{URL::asset('Sorry.mp4')}}" type="video/mp4">
		</video>	
 -->
		<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_PI/sdk.js#xfbml=1&version=v2.7&appId=818252901657743";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</div>

	<div class="videoinfo">


			<div class="single-list-container">

			{!!$list->content!!}
	</div>

<div class="fb-like" data-href="https://www.facebook.com/Hypedgamers/" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="true"></div>
		


	</div>
<div class="videoRelatedWrapper">
	<div class="containerRelated">
	<h3 class="RelatedVideoText">Related Videos</h3>
		@foreach($post as $post)
		<div class="videoImg">
	    	<div class="thumb">
				<a href="/list/{{$post->slug}}">
					<span class="play">&#9658;</span>
					<div class="overlay"></div>
				</a>
				<img border="0" alt="{{$post->title}}" src="//fiska.hypedgamers.com/uploads/{{$post->thumbnail}}">
			</div>
			<a href="/list/{{$post->slug}}"> {!!$post->title!!}</a>

			<p>Posted : {{ date('F d, Y', strtotime($post->created_at)) }}</p>
		</div>

		@endforeach
		
</div>		
</div>

@stop