@extends('layout')

@section('content')

<div class="submitContainer">

	<div class="submitWrapper">
			<!--<form role="form">
		  <div class="form-group">
		    <label for="email">Email address:</label>
		    <input type="email" class="form-control" id="email">
		  </div>
		  <button type="submit" class="btn btn-default">Submit</button>
		</form>-->
		<ul>
	    @foreach($errors->all() as $error)
	        <li>{{ $error }}</li>
	    @endforeach
	    @if(Session::has('message'))
		    <div class="alert alert-info">
		      {{Session::get('message')}}
		    </div>
		@endif
	</ul>

	{!! Form::open(array('route' => 'submit_store', 'role' => 'form')) !!}

	<div class="form-group">
	    {!! Form::label('URL*') !!}
	    {!! Form::text('url', null, 
	        array('required', 
	              'class'=>'form-control', 
	              'placeholder'=>'Enter URL')) !!}
	</div>

	<div class="form-group">
	    {!! Form::label('Game name*') !!}
	    {!! Form::text('game', null, 
	        array('required', 
	              'class'=>'form-control', 
	              'placeholder'=>'Enter game name')) !!}
	</div>

	<div class="form-group">
	    {!! Form::label('Video Title') !!}
	    {!! Form::text('title', null, 
	        array('max:500', 
	              'class'=>'form-control', 
	              'placeholder'=>'Optional')) !!}
	</div>

	<div class="form-group">
	    {!! Form::label('Description') !!}
	    {!! Form::textarea('message', null, 
	        array('max:500', 
	              'class'=>'form-control', 
	              'placeholder'=>'Optional')) !!}
	</div>

	<div class="form-group">
	    {!! Form::submit('Send Video', 
	      array('class'=>'btn btn-primary')) !!}
	</div>
	{!! Form::close() !!}
	</div>

	<!--
	<div class="submit">
		<form class="formSubmit" action="/videoForm">
			URL*<br>
			<input type="text" name="URL"><br>
			Game name*<br>
			<input type="text" name="GameName"><br>
			Video title*<br>
			<input type="text" name="VideoTitle"><br>
			Description<br>
			<textarea name="message" rows="10" cols="30"></textarea><br>
			<input type="submit">
		</form>
	</div>-->
</div>
@stop