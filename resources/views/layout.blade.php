<!DOCTYPE html>
<html lang="en">
<head>

   @if(!empty($seo))
       <title>{{$seo->title}}</title>
        <meta name="Description" CONTENT="{{$seo->content}}">
        <meta property="og:title"       content="{{$seo->title}}"/>
        <meta property="og:description" content="{{strip_tags($seo->content)}}"/>
        @if(!empty($seoImage))
                <meta property="og:image"       content="//fiska.hypedgamers.com/uploads/{{$seoImage}}"/>
        @else
                <meta property="og:image"       content="http://hypedgamers.com/default-share.jpg"/>
        @endif
   @else
        <title>HypedGamers - 404 not found</title>
        <meta name="Description" CONTENT="Sorry, but we cant find what you are looking for!">
        <meta property="og:title"       content="HypedGamers - 404 not found"/>
        <meta property="og:description" content="Sorry, but we cant find what you are looking for!"/>
        <meta property="og:image"       content="http://hypedgamers.com/default-share.jpg"/>
    @endif
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="http://vjs.zencdn.net/5.10.7/video-js.css" rel="stylesheet">

  	<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script> 

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://use.fontawesome.com/241e97cee5.js"></script>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans|Cuprum:400italic|Asap:700' rel='stylesheet' type='text/css'>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script type="text/javascript" src="{{ URL::asset('js/javascript.js') }}"></script>

    <link href="/css/app.css" rel="stylesheet">

<!-- Hotjar Tracking Code for http://hypedgamers.com/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:266240,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '123438738024352');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=123438738024352&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body>
<div class="app-container">

	<header> 
	  	<div class="container">
	
	  		@if(!empty($trending))
		    <div id="myCarousel" class="carousel slide" data-ride="carousel">
		      <!-- Indicators -->
				<ol class="carousel-indicators">
			        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			        <li data-target="#myCarousel" data-slide-to="1"></li>
		      	</ol>
	
		      <!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
			
			   		     
						@foreach($trending as $index=>$sliderItem)
							@if($index === 0)
							<div class="item active">
							
							@elseif($index===0 || $index === 4 )
								<div class="item">
							@endif
					
								<div class="imageDiv">
									<a href="@if($sliderItem->type==='posts')/video/@else/	list/@endif{{$sliderItem->slug}}
										">
										<span class="play">&#9658;</span>
										<div class="overlay"></div>
									</a>
									<img class="lazy"  data-src="//fiska.hypedgamers.com/	uploads/{{$sliderItem->thumbnail}}?w=350" alt="	{{$sliderItem->	title}}">
									<div class="caption-1">
									
										<h3>{{$sliderItem->title}}<br/></h3>
									</div>
								</div>
							@if($index===0 || $index === 4)
					
								</div>
							@endif
						@endforeach
			        </div>
		      <!-- Left and right controls -->
		      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="	prev">
		        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		        <span class="sr-only">Previous</span>
		      </a>
		      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="	next">
		        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		        <span class="sr-only">Next</span>
		      </a>
		    </div>
		    @endif
	 	</div>
	
<div class="nav-container">
<nav class="navbar navbar-default">
<div class="container-fluid">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="/"><img src="{{URL::asset('/images/	logotyp.png')}}" alt="HypedGamers logotype."></a>
</div>

<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<li><a href="/">Home <span class="sr-only">(current)</span></a></li>
<!--<li><a href="/Lists">Lists </a></li>-->
<li><a href="/Videos">Videos</a></li>
<li><a href="/Submit">Submit Video</a></li>
<!-- <li><a href="/Contact">Contact us</a></li> -->
</ul>
<ul class="nav navbar-nav navbar-right">
<li><a  target="_blank" href="https://www.facebook.com/Hypedgamers	" class="FB"><i class="fa fa-facebook" aria-hidden="true"></i></a>	</li>
</ul>
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
</div> <!-- nav-container -->

	</header>
		
	
		@yield('content')
	
	
	<script type="text/javascript" src="/js/unveil/jquery.unveil.js"> </script>
	<script>
	$(document).ready(function() {
	$(".lazy").unveil(200, function() {
	  $(this).load(function() {
	    this.style.opacity = 1;
	    $(".videosImages").css('opacity','1');
	  });
	});
	});
	</script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-58459850-4', 'auto');
	  ga('send', 'pageview');
	</script>
</div> <!-- app-container -->
</body>

</html>