<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class list_views extends Model
{

	static function store($id)
	{
		$view = new list_views();
		$view->list_id = $id;
		return $view->save();
	}
}
