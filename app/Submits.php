<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submits extends Model
{


	static function store($request)
	{	

		$sub = new Submits();
		$sub->title = $request->title;
		$sub->url = $request->url;
		$sub->message = $request->message;
		$sub->game = $request->game;
		return $sub->save();
	}
}
