<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportViews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:views';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports views of each list / post every user has visited for the last 15 minutes.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app('App\Http\Controllers\ImportRedisController')->importViews();
    }
}
