<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lists extends Model
{
 protected $fillable = array('title', 'content', 'thumbnail', 'slug','created_at','updated_at','id');
}
