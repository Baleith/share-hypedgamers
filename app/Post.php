<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $fillable = array('title', 'content', 'videoUrl', 'tag_id', 'category_id', 'thumbnail','slug');

   
   static function get($slug)
   {
   		$post = post::where('slug','=',$slug);

   		return $post;
   }

   public function category()
   {
   		return $this->hasOne('App\categories',"id","category_id");
   }
    public function tags()
   {
   		return $this->hasOne('App\tags',"id","tag_id");
   }
}