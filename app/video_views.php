<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class video_views extends Model
{

	static function store($id)
	{
		$view = new video_views();
		$view->post_id = $id;
		return $view->save();
	}
}
