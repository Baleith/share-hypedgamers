<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
class redisHelper extends Model
{
	public static function addArrayToRedis($key, $data)
	{	
		$current = Redis::get($key) ?? [];

		if(empty($current)){
			$current[] = $data;
			$current = json_encode($current);
		}
		else{
			$current = substr($current, 0, -1);
	    	$current .= "," . json_encode($data) . "]";
		}
		
	    Redis::set($key, $current);
	}

	public static function getArrayFromRedis($key)
	{
		return json_decode(Redis::get($key));
	}

}
