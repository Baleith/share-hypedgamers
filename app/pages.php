<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
	  protected $fillable = array('name', 'seo_id');


	  public function seo()
	  {
	  	return $this->belongsTo('App\Seo','seo_id','id');
	  }
}
