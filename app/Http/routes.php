<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/test',"ImportRedisController@importViews");

Route::get('/', 'PagesController@home');

Route::get('/Videos', 'PagesController@Videos');

//Route::get('/Lists', 'PagesController@Lists');
//Route::get('/list/{slug}', 'PagesController@showList');


Route::get('/video/{slug}', 'PagesController@videoclicked');

Route::get('/Submit', 'PagesController@Submit');

Route::get('/filter/js/{type}','PagesController@filterAjax');

Route::post('submit', ['as' => 'submit_store', 'uses' => 'SubmitController@store']);

//Route::get('/Contact', 'PagesController@Contact');

//Route::get('/Contact', ['as' => 'contact', 'uses' => 'AboutController@create']);

Route::post('contact', ['as' => 'contact_store', 'uses' => 'SubmitController@store']);
