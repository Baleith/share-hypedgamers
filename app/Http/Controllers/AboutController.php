<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Mail;
class AboutController extends Controller
{
    public function create()
    {
    	return view('contact');
    }

    public function store(Request $request)
    {
    	$this->validate($request, ['name' => 'required',
            'email' => 'required|email',
            'message' => 'required']);
    		
    		Mail::send('emails.contact', array(
	            'name' => $request->get('name'),
	            'email' => $request->get('email'),
	            'user_message' => $request->get('message')
	        ), function($message)
	    {
	        $message->from('wj@wjgilmore.com');
	        $message->to('wj@wjgilmore.com', 'Admin')->subject('TODOParrot Feedback');
	    });    



            die();
		        return \Redirect::route('contact')
      ->with('message', 'Thanks for contacting us!');
    }
}
