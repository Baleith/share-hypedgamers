<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redis;
use App\redisHelper;
use App\video_views;
use App\list_views;
class ImportRedisController extends Controller
{
	public function importViews()
	{
		$siteViews = redisHelper::getArrayFromRedis('site:views');


		if($siteViews)
		{
			foreach($siteViews as $key => $view)
			{		

				if($view->key == "list_view")
				{
					list_views::store($view->id);
				}
				elseif($view->key=="post_view")
				{
					video_views::store($view->id);
				}
			}
		}

		// Delete 
		$siteViews = null;
		Redis::del('site:views');
	}
}
