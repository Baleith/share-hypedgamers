<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use App\tags;
use Redirect;
use DB;
use App\video_views;
use App\Lists;
use App\list_views;
use App\Pages;
use App\redisHelper;
class PagesController extends Controller
{	

    public function home()
    {	
    	$Pages = Pages::where('name','=','home')->with('seo')->first();
    	$seo = $Pages->seo;
		$trending = $this->getSlider();
		$posts = Post::orderBy("created_at",'DESC')->get();
        return view('home',array('posts'=>$posts,'trending'=>$trending,'seo'=>$seo));
	}

	public function Videos()
    {
    	$Pages = Pages::where('name','=','videos')->with('seo')->first();
    	if(!empty($Pages->seo))
    	{
    		$seo = $Pages->seo;
    	}
    	else
    	{
    		$seo=array();
    	}
    	$slider = $this->getSlider();
      	$posts = post::with('category')->with("tags")->orderBy('created_at','desc')->get();

		return view('Videos',array('posts' => $posts,'trending'=>$slider,'seo'=>$seo));
	}

	public function Lists()
	{	
		$Pages = Pages::where('name','=','lists')->with('seo')->first();
		if(!empty($Pages->seo))
		{
			$seo = $Pages->seo;
		}
		else
		{
			$seo = array();
		}
		$slider = $this->getSlider();
		$lists = Lists::orderBy('created_at','desc')->get();
		return view('lists.showLists',array('lists' => $lists,"trending" => $slider,'seo'=>$seo));
	}

	public function showList($slug)
	{	
		$posts = Lists::orderBy('created_at',"desc")->get();
		$slider = $this->getSlider();

		$list = Lists::where('slug','=',$slug)->first();
		if(empty($list))
		{
			return Redirect::to("/404",array('trending' => $slider));
		}
		$seo = (object) array(
		'title'=>$list->title,
		'content'=>$list->content,
		);
		$seoImage = $list->thumbnail;	
    	$this->addView('list_view',$list->id);
		return view('lists.singleList',array('list'=>$list,'post' => $posts,'trending'=>$slider,'seo'=>$seo,'seoImage' => $seoImage));
	}	

	public function videoclicked($slug)
    {	
    	$slider = $this->getSlider();
    	$post = post::with('category')->with("tags")->where('slug','=',$slug)->first();
    	if(empty($post))
    	{
    		return Redirect::to("/404",array('trending'=>$slider));
    	}
    	$seo = (object) array(
		'title'=>$post->title,
		'content'=>$post->content,
		);
		$seoImage = $post->thumbnail;	
    	$posts = post::with('category')->with("tags")->get();
    	$this->addView('post_view',$post->id);
		return view('videoclicked',array('post'=>$post,"posts"=>$posts,'trending'=>$slider,'seo'=>$seo,'seoImage' => $seoImage));
	}

	public function Submit()
    {			
    	$Pages = Pages::where('name','=','submit')->with('seo')->first();
    	if(!empty($Pages->seo))
		{
			$seo = $Pages->seo;
		}
		else
		{
			$seo = array();
		}

    	$slider = $this->getSlider();
		return view('submit',array('trending'=>$slider,'seo'=>$seo));
	}

	public function getNewestMixed()
	{

		return $posts = DB::select( DB::raw("
			SELECT title,created_at,slug,thumbnail, 'posts' as type FROM posts
			"));
	}

	public function getTrendingPosts()
	{
		return DB::select( DB::raw("
			SELECT posts.title,posts.created_at,posts.slug,posts.thumbnail,posts.id as parentid, 'posts' as type,
			(SELECT count(video_views.id)
			FROM video_views
			WHERE parentid = video_views.post_id) as views
			FROM posts
			WHERE posts.created_at <= DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 WEEK)), INTERVAL 1 DAY) 
			GROUP BY id
			ORDER BY views DESC
			"));
	}
	public function getPopularPosts()
	{
			return DB::select( DB::raw("
			SELECT posts.*,count(video_views.id) as views FROM posts, video_views WHERE posts.id = video_views.post_id 
			GROUP BY posts.id
			ORDER BY views DESC
			"));
	}
		public function getPopularMixed()
	{

				return DB::select( DB::raw("
					SELECT posts.title,posts.created_at,posts.slug,posts.thumbnail,posts.id as parentid, 'posts' as type,
						(SELECT count(video_views.id)
						FROM video_views
						WHERE parentid = video_views.post_id) as views
						FROM posts
					GROUP BY id
					ORDER BY views DESC
				"));

			
	}

	public function getSlider()
	{
		return DB::select( DB::raw("
			SELECT posts.title,posts.created_at,posts.slug,posts.thumbnail,posts.id as parentid, 'posts' as type,
			(SELECT count(video_views.id)
			FROM video_views
			WHERE parentid = video_views.post_id) as views
			FROM posts
			WHERE posts.created_at <= DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 WEEK)), INTERVAL 1 DAY) 
			limit 4
		"));
	}
	public function getTrendingMixed()
	{	
			return DB::select( DB::raw("
			SELECT posts.title,posts.created_at,posts.slug,posts.thumbnail,posts.id as parentid, 'posts' as type,
			(SELECT count(video_views.id)
			FROM video_views
			WHERE parentid = video_views.post_id) as views
			FROM posts
			WHERE posts.created_at <= DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 WEEK)), INTERVAL 1 DAY) 
			GROUP BY id
			ORDER BY views DESC
		"));
		
	}
	public function filterAjax($type)
	{	
		if($type==="popular")
		{
			$posts = $this->getPopularPosts();
		}
		elseif($type==="newest")
		{
			$posts = Post::orderBy("created_at",'DESC')->get();
		}
		elseif($type==="trending")
		{
			$posts = $this->getTrendingPosts();
		}
		elseif($type==="popular-mixed")
		{
			$posts = $this->getPopularMixed();
		}
		elseif($type==="newest-mixed")
		{
			$posts = $this->getNewestMixed();
		}
		elseif($type==="trending-mixed")
		{
			$posts = $this->getTrendingMixed();
		}
		else
		{
			$posts = array();
		}

		return view('ajax.video', array('posts' => $posts))->render();
	}

	public function addView($key,$value)
	{
		redisHelper::addArrayToRedis('site:views', 
				$siteViews = 
				[	
					'key' => $key,
					'id' => $value
				]
			);

	}
	/*public function Contact()
    {
		return view('contact');
	}

	public function mailUs() {
		return view('Videos');
	}*/
}
