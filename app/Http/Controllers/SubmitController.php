<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Submits;
use Redirect;
class SubmitController extends Controller
{
	public function store(Request $request)
	{
		Submits::store($request);

		return Redirect::back()->with('message',"Successfully posted a tip. Thanks!");
	}

}
